package cs200_PA5;

/* concept from "Data Abstraction and Problem Solving," Prichard, 605 */
public class TreeNode<T> {
	
	T payload;
	TreeNode<T> rightChild;
	TreeNode<T> leftChild;
	
	/**
	 * 
	 * @param inputPayload
	 * Object to be "carried" by node
	 * @param left
	 * TreeNode to be connected as left child
	 * @param right
	 * TreeNode to be connected as right child
	 */
	TreeNode(T inputPayload, TreeNode<T> left, TreeNode<T> right)
	{
		payload = inputPayload;
		rightChild = right;
		leftChild = left;
	}
}
