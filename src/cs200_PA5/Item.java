package cs200_PA5;

public class Item
{
	int a;
	Item(int A)
	{
		a = A;
	}
	@Override
	public boolean equals(Object b)
	{
		if(b instanceof Item)
		{
			if(((Item)b).a == a)
			{
				return true;
			}
		}
		return false;
	}
}
