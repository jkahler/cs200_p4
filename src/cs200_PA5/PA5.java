package cs200_PA5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class PA5
{
	public static void main(String[] args)
	{		
		WebPages searchEngine = null; 								// WebPages object for current "search engine"
		String currentInput = null; 								// holds current input from inStream
		ArrayList<String> stopWords = new ArrayList<String>(); 		// stopwords to remove
		ArrayList<ArrayList<String>> queries = new ArrayList<ArrayList<String>>(); 	// list of queries to calculate with bestPages()
		ArrayList<String> tempQuery = null;
		boolean fileNamesStop = false; 								// true if file still taking in page names
		boolean stopWordsStop = false;
		String graphFileName = null;
		
		// Add pages and read words to check
		try
		{
			Scanner inStream = new Scanner(new File(args[0])); 		// file containing input filenames and directives

			//store first value as hash size
			if(inStream.hasNext())
			{
				graphFileName = inStream.next();
			}
			else
			{
				System.err.println("No output file name specified in input file");
				System.exit(0);
			}
			if(inStream.hasNext())
			{
				if(inStream.hasNextInt())							
				{
					searchEngine = new WebPages(inStream.nextInt());
				}
				else
				{
					System.err.println("Problem reading input file: no hash table size");
					System.exit(0);
				}
			}
			else
			{
				System.err.println("Problem reading input file: empty file");
				System.exit(0);
			}

			//read in file names
			while (fileNamesStop == false) 			
			{
				if (!inStream.hasNext())
				{
					System.err.println("Problem reading input file: does not contain *EOFs*");
					System.exit(0);
				}
				currentInput = inStream.next();
				if (currentInput.contentEquals("*EOFs*"))
				{
					fileNamesStop = true;
				} else
				{
					inStream.nextLine();
					searchEngine.addPage(currentInput);
				}
			}
			//read in stop words
			while (!stopWordsStop)					
			{
				if (!inStream.hasNext())
				{
					System.err.println("Problem reading input file: does not contain *STOPs*");
					System.exit(0);
				}
				currentInput = inStream.next();
				if(currentInput.equals("*STOPs*"))
				{
					stopWordsStop = true;
				}
				else
				{
					stopWords.add(currentInput.toLowerCase());
				}
				
			}
			//read in queries
			if(inStream.hasNextLine())
			{
				inStream.nextLine();
			}
			else
			{
				System.err.println("missing queries!");
				System.exit(0);
			}
			
			while(inStream.hasNextLine())
			{
				tempQuery = new ArrayList<String>(Arrays.asList(inStream.nextLine().split("\\s")));
				for(int i = 0 ; i < tempQuery.size() ; i++)
				{
					tempQuery.set(i, tempQuery.get(i).toLowerCase());
				}
				//sort array
				Collections.sort(tempQuery);
				if(tempQuery.size() > 0)
				{
					queries.add(tempQuery);
				}
				
			}
			inStream.close();

		} catch (FileNotFoundException e)
		{
			System.err.println("File not found!");
			System.exit(0);
		} catch (IndexOutOfBoundsException e)
		{
			System.err.println("Missing argument for input file");
			System.exit(0);
		}
		//prune stopwords
		searchEngine.pruneStopWords(stopWords);
		
		//Generate Dot Map
		searchEngine.makeDotFile(graphFileName);
		for( ArrayList<String> q : queries)
		{
			System.out.print("[");
			for(int i = 0 ; i < q.size() ; i++)
			{
				System.out.print(q.get(i) + " " );
			}
			if (searchEngine.bestPage(q) == null){
				System.out.println("] not found");
			}
			else{
				System.out.print("] in ");
				System.out.println(searchEngine.bestPage(q));
			}
			
		}
		
	}
}
