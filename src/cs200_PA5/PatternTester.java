package cs200_PA5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternTester
{
	public static void main(String[] args)
	{
		String tempResult = null;
		String text  = "asd;lfkj test <      a     >  < a\n  \t href = \"http://testfile.txt\"  \n\t > < a href> < a  \t href = \"\" sillytimes"
				+ "<a href=\"\">" + "<a href=\"http://\">";

		String patternString = "<\\s*a\\s+href\\s*=\\s*\"http://([^\"]*)\"\\s*>";

		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(text);

		int count = 0;
		while(matcher.find()) {
		    count++;
		    System.out.println("found: " + count + " : " + text.substring(matcher.start(), matcher.end()));
		    tempResult = matcher.group(1);
		    System.out.println(tempResult);
		}

	}

}
